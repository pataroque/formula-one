import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constructor } from '../models/constructor';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Driver} from '../models/driver';

@Injectable()
export class ConstructorService {

  endpoint = 'https://ergast.com/api/f1/2016/';
  reponseType = '.json';

  constructor(
    private http: HttpClient,
  ) { }

  get(): Observable<Constructor[]> {
    return this.http
      .get( this.endpoint + `constructors` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.ConstructorTable.Constructors)
      );
  }

  getBy(id: any): Observable<Driver> {
    return this.http
      .get( this.endpoint + `constructors/${id}` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.ConstructorTable.Constructors[0])
      );
  }
}
