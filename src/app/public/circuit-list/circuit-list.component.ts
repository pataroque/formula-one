import { Component, OnInit } from '@angular/core';
import { Circuit } from '../shared/models/circuit';
import { CircuitService } from '../shared/services/circuit.service';

@Component({
  selector: 'app-circuit-list',
  templateUrl: './circuit-list.component.html',
  styleUrls: ['./circuit-list.component.scss']
})
export class CircuitListComponent implements OnInit {

  circuits: Array<Circuit> = [];

  constructor(
    private circuitService: CircuitService
  ) { }

  ngOnInit() {
    this.getCircuits();
  }

  getCircuits() {
    this.circuitService
      .get()
      .subscribe(
        res => {
          this.circuits = res;
        },
        err => {
          alert(err);
        }
      );
  }

}
