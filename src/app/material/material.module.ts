import { NgModule } from '@angular/core';
import {
  MatCardModule,
  MatDividerModule,
  MatTabsModule,
  MatToolbarModule,
  MatProgressBarModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule
} from '@angular/material';

@NgModule({
  imports: [
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule
  ]
})
export class MaterialModule { }
