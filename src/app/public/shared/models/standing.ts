import { Driver } from './driver';
import { Constructor } from './constructor';

export interface Standing {
  Constructors: Array<Constructor>;
  Driver: Driver;
  points: string;
  position: string;
  positionText: string;
  wins: string;
}
