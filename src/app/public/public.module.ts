import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { DriverListComponent } from './driver-list/driver-list.component';
import { ConstructorListComponent } from './constructor-list/constructor-list.component';
import { CircuitListComponent } from './circuit-list/circuit-list.component';
import { SharedModule } from '../shared/shared.module';
import { DriverService } from './shared/services/driver.service';
import { ConstructorService } from './shared/services/constructor.service';
import { CircuitService } from './shared/services/circuit.service';
import { MaterialModule } from '../material/material.module';
import { DriverDetailsComponent } from './driver-details/driver-details.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    PublicRoutingModule
  ],
  declarations: [
    DriverListComponent,
    ConstructorListComponent,
    CircuitListComponent,
    DriverDetailsComponent
  ],
  providers: [
    DriverService,
    ConstructorService,
    CircuitService
  ]
})
export class PublicModule { }
