import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverListComponent } from './driver-list/driver-list.component';
import { ConstructorListComponent } from './constructor-list/constructor-list.component';
import { CircuitListComponent } from './circuit-list/circuit-list.component';
import { DriverDetailsComponent } from './driver-details/driver-details.component';

const routes: Routes = [
  { path: 'circuits', component: CircuitListComponent },
  { path: 'constructors', component: ConstructorListComponent },
  { path: 'drivers', component: DriverListComponent },
  { path: 'drivers/:id', component: DriverDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
