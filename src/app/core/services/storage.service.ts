import { Injectable } from '@angular/core';
import {Driver} from '../../public/shared/models/driver';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public static clear(): void {
    localStorage.clear();
  }

  public static addDriver(driver: Driver): void {
    const driversString = localStorage.getItem('lastFiveVisitedDrivers');

    const drivers = driversString ? JSON.parse(driversString) : [];

    drivers.push(driver);

    if (drivers.length > 5) {
      drivers.shift();
    }

    localStorage.setItem('lastFiveVisitedDrivers', JSON.stringify(drivers));
  }

  public static getDriver(id: string): Driver {
    let recentDriver: Driver;

    const drivers = JSON.parse(localStorage.getItem('lastFiveVisitedDrivers'));

    if ( drivers && drivers.length ) {
      recentDriver = drivers.filter( driver => driver.driverId === id)[0];
    }

    return recentDriver;
  }

  public static getDrivers(): Driver[] {
    return JSON.parse(localStorage.getItem('lastFiveVisitedDrivers'));
  }
}
