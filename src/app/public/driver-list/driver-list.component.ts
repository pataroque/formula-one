import { Component, OnInit } from '@angular/core';
import { DriverService } from '../shared/services/driver.service';
import { Standing } from '../shared/models/standing';

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.scss']
})
export class DriverListComponent implements OnInit {

  standings: Array<Standing> = [] ;

  constructor(
    private driverService: DriverService
  ) { }

  ngOnInit() {
    this.getStandings();
  }

  getStandings() {
    this.driverService
      .getStandings()
      .subscribe(
        res => {
          this.standings = res;
        },
        err => {
          alert(err);
        }
      );
  }

}
