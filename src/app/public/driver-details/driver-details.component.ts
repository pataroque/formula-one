import { Component, OnInit } from '@angular/core';
import { Driver } from '../shared/models/driver';
import { DriverService } from '../shared/services/driver.service';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'app-driver-details',
  templateUrl: './driver-details.component.html',
  styleUrls: ['./driver-details.component.scss']
})
export class DriverDetailsComponent implements OnInit {

  id: string;
  driver: Driver;
  lastFiveDrivers: Array<Driver> = [];
  recent = false;

  constructor(
    private driverService: DriverService,
    private route: ActivatedRoute
  ) {  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.id = params['id'];
      this.lastFiveDrivers = StorageService.getDrivers();
      this.getDriver();
    });
  }

  getDriver() {

    const driver = StorageService.getDriver(this.id);

    if (driver && driver.driverId) {
      this.driver = driver;
    } else {

      this.driverService
        .getBy(this.id)
        .subscribe(
          res => {
            this.driver = res;
            StorageService.addDriver(this.driver);
          },
          err => {
            alert(err);
          }
        );
    }

  }

}
