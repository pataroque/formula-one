import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Driver } from '../models/driver';
import { Standing } from '../models/standing';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DriverService {

  endpoint = 'https://ergast.com/api/f1/2016/';
  reponseType = '.json';

  constructor(
    private http: HttpClient,
  ) { }

  get(): Observable<Driver[]> {
    return this.http
      .get( this.endpoint + `drivers` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.DriverTable.Drivers)
      );
  }

  getBy(id: any): Observable<Driver> {
    return this.http
      .get( this.endpoint + `drivers/${id}` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.DriverTable.Drivers[0])
      );
  }

  getStandings(): Observable<Standing[]> {
    return this.http
      .get( this.endpoint + `driverStandings` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.StandingsTable.StandingsLists[0].DriverStandings)
      );
  }
}
