import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Circuit } from '../models/circuit';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CircuitService {

  endpoint = 'https://ergast.com/api/f1/2016/';
  reponseType = '.json';

  constructor(
    private http: HttpClient,
  ) { }

  get(): Observable<Circuit[]> {
    return this.http
      .get( this.endpoint + `circuits` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.CircuitTable.Circuits)
      );
  }

  getBy(id: any): Observable<Circuit> {
    return this.http
      .get( this.endpoint + `circuits/${id}` + this.reponseType)
      .pipe(
        map((data: any) => data.MRData.CircuitTable.Circuits[0])
      );
  }
}
