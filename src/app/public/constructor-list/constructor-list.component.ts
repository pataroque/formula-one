import { Component, OnInit } from '@angular/core';
import { Constructor } from '../shared/models/constructor';
import { ConstructorService } from '../shared/services/constructor.service';

@Component({
  selector: 'app-constructor-list',
  templateUrl: './constructor-list.component.html',
  styleUrls: ['./constructor-list.component.scss']
})
export class ConstructorListComponent implements OnInit {

  constructors: Array<Constructor> = [];

  constructor(
    private constructorService: ConstructorService
  ) { }

  ngOnInit() {
    this.getConstructors();
  }

  getConstructors() {
    this.constructorService
      .get()
      .subscribe(
        res => {
          this.constructors = res;
        },
        err => {
          alert(err);
        }
      );
  }

}
